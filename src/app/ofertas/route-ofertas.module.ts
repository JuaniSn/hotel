import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { OfertasComponent } from './ofertas.component';



const ofertaRouting: Routes = [
	{path: 'ofertas', component: OfertasComponent }	
];



@NgModule({
	imports: [
	 	RouterModule.forChild(ofertaRouting)
	],
	exports: [
		RouterModule
	]
})

export class RouteOfertas {}