import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouteOfertas } from './route-ofertas.module';



import { OfertasComponent } from './ofertas.component';

@NgModule({
  imports: [
    CommonModule,
    RouteOfertas
  ],
  declarations: [OfertasComponent]
})
export class OfertasModule { }
