import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// modulos 
import { AlojamientoModule } from './alojamiento/alojamiento.module';
import { CelebracionesModule } from './celebraciones/celebraciones.module';
import { ComidasBebidasModule } from './comidas-bebidas/comidas-bebidas.module';
import { EstiloVidaModule } from './estilo-vida/estilo-vida.module';
import { BodasModule } from './bodas/bodas.module';
import { OfertasModule } from './ofertas/ofertas.module';
import { AuthModule } from './auth/auth.module';

// rutas
import { RoutingModule } from './routing.module';

// componentes generales
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './navegacion/header/header.component';
import { SidenavComponent } from './navegacion/sidenav/sidenav.component';
import { NavtabsComponent } from './navegacion/navtabs/navtabs.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    SidenavComponent,
    NavtabsComponent
  ],
  imports: [
    BrowserModule,
    AlojamientoModule,
    CelebracionesModule,
    ComidasBebidasModule,
    EstiloVidaModule,
    BodasModule,
    OfertasModule,
    AuthModule,
    RoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
