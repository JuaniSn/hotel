import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouteAuth } from './route-auth.module';
 
import { IniciarSesionComponent } from './componentes/iniciar-sesion/iniciar-sesion.component';
import { UneteComponent } from './componentes/unete/unete.component';
import { AuthComponent } from './auth.component';

@NgModule({
  imports: [
    CommonModule,
    RouteAuth
  ],
  declarations: [IniciarSesionComponent, UneteComponent, AuthComponent]
})
export class AuthModule { }
