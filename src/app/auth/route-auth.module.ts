import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IniciarSesionComponent } from './componentes/iniciar-sesion/iniciar-sesion.component';
import { UneteComponent } from './componentes/unete/unete.component';



const authRouting: Routes = [
	{path: 'iniciar-sesion', component: IniciarSesionComponent },	
	{path: 'registrarse', component: UneteComponent }	
];



@NgModule({
	imports: [
	 	RouterModule.forChild(authRouting)
	],
	exports: [
		RouterModule
	]
})

export class RouteAuth {}