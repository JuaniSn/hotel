import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ComidasBebidasComponent } from './comidas-bebidas.component';


const comidasRouting: Routes = [
	{path: 'comidas-y-bebidas', component: ComidasBebidasComponent }	
];



@NgModule({
	imports: [
	 	RouterModule.forChild(comidasRouting)
	],
	exports: [
		RouterModule
	]
})

export class RoutesComidas {}