import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoutesComidas } from './routes-comidas.module';

import { RestauranteComponent } from './componentes/restaurante/restaurante.component';
import { BarComponent } from './componentes/bar/bar.component';
import { ComidasBebidasComponent } from './comidas-bebidas.component';

@NgModule({
  imports: [
    CommonModule,
    RoutesComidas
  ],
  declarations: [RestauranteComponent, BarComponent, ComidasBebidasComponent]
})
export class ComidasBebidasModule { }
