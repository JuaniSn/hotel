import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouteEstiloVida } from './route-estilo-vida.module';



import { BienestaSpaComponent } from './componentes/bienesta-spa/bienesta-spa.component';
import { EstiloVidaComponent } from './estilo-vida.component';
import { ActividadesResortComponent } from './componentes/actividades-resort/actividades-resort.component';
import { ExperienciasComponent } from './componentes/experiencias/experiencias.component';
import { ExcursionesComponent } from './componentes/excursiones/excursiones.component';

@NgModule({
  imports: [
    CommonModule,
    RouteEstiloVida
  ],
  declarations: [BienestaSpaComponent, EstiloVidaComponent, ActividadesResortComponent, ExperienciasComponent, ExcursionesComponent]
})
export class EstiloVidaModule { }
