import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EstiloVidaComponent } from './estilo-vida.component';



const estiloVidaRouting: Routes = [
	{path: 'estilo-de-vida', component: EstiloVidaComponent }	
];



@NgModule({
	imports: [
	 	RouterModule.forChild(estiloVidaRouting)
	],
	exports: [
		RouterModule
	]
})

export class RouteEstiloVida {}