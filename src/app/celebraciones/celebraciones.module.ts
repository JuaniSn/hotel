import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoutesCelebraciones } from './route-celebraciones.module';

import { CelebracionesComponent } from './celebraciones.component';


@NgModule({
  imports: [
    CommonModule,
    RoutesCelebraciones
  ],
  declarations: [CelebracionesComponent]
})
export class CelebracionesModule { }
