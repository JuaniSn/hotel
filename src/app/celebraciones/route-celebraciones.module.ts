import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CelebracionesComponent } from './celebraciones.component';


const celebracionesRouting: Routes = [
	{path: 'celebraciones', component: CelebracionesComponent }	
];



@NgModule({
	imports: [
	 	RouterModule.forChild(celebracionesRouting)
	],
	exports: [
		RouterModule
	]
})

export class RoutesCelebraciones {}