import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouteBodas } from './route-bodas.module';



import { BodasComponent } from './bodas.component';
import { TailandesasComponent } from './componentes/tailandesas/tailandesas.component';
import { OccidentalesComponent } from './componentes/occidentales/occidentales.component';

@NgModule({
  imports: [
    CommonModule,
    RouteBodas
  ],
  declarations: [BodasComponent, TailandesasComponent, OccidentalesComponent]
})
export class BodasModule { }
