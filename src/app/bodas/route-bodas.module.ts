import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BodasComponent } from './bodas.component';



const bodaRouting: Routes = [
	{path: 'bodas', component: BodasComponent }	
];



@NgModule({
	imports: [
	 	RouterModule.forChild(bodaRouting)
	],
	exports: [
		RouterModule
	]
})

export class RouteBodas {}