import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AlojamientoComponent } from './alojamiento.component';
import { HabitacionesComponent } from './componentes/habitaciones/habitaciones.component';


const alojamientoRouting: Routes = [
	{path: 'alojamiento', component: AlojamientoComponent }	
];



@NgModule({
	imports: [
	 	RouterModule.forChild(alojamientoRouting)
	],
	exports: [
		RouterModule
	]
})

export class RoutesAlojamiento {}