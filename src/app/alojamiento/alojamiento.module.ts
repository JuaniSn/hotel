import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoutesAlojamiento } from './routes-alojamiento.module';

import { AlojamientoComponent } from './alojamiento.component';
import { HabitacionesComponent } from './componentes/habitaciones/habitaciones.component';



@NgModule({
  imports: [
    CommonModule,
    RoutesAlojamiento
  ],
  declarations: [
    AlojamientoComponent,
    HabitacionesComponent
  ]
})
export class AlojamientoModule { }
