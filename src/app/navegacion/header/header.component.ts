import { Component, OnInit, EventEmitter, Output } from '@angular/core';
declare var M:any;

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  	M.AutoInit();
  }

}
