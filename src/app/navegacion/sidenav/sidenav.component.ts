import { Component, OnInit } from '@angular/core';
declare var M:any;

@Component({
  selector: 'sidenav-lista',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(){
  	  var elems = document.querySelectorAll('.sidenav');
   	  var instances = M.Sidenav.init(elems);
  }

}
